package com.example.bata.neshantest;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBasetTest extends SQLiteOpenHelper {


    public static  final  String  My_data_base ="my_database_place.db";
    public static  final  String  tbl_pl ="plinfo";
    Context context;
    public DataBasetTest( Context context) {
        super(context, My_data_base, null, 1);
        SQLiteDatabase db = this.getWritableDatabase();
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + tbl_pl + " (Id INTEGER PRIMARY KEY AUTOINCREMENT ,userX double,userY double,lineX1 double,lineY1 double,lineX2 double,lineY2 double)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+tbl_pl);
    }
    public Boolean insert ( double x1 ,double y1,double x2,double y2,double x3,double y3)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues doc = new ContentValues();
        doc.put("userX" ,x1);
        doc.put("userY" ,y1);
        doc.put("lineX1" ,x2);
        doc.put("lineY1" ,y2);
        doc.put("lineX2" ,x3);
        doc.put("lineY2" ,y3);
        long result = db.insert(tbl_pl ,null ,doc);
        if (result== -1)
        {
            return false;
        }
        else
            return true;
    }
    public Cursor show_all()
    {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cr =db.rawQuery("select * from "+tbl_pl,null);
        return cr;
    }

}
