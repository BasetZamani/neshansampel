package com.example.bata.neshantest;

import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import org.neshan.core.LngLat;
import org.neshan.core.Range;
import org.neshan.core.Variant;
import org.neshan.layers.Layer;
import org.neshan.layers.VectorElementLayer;
import org.neshan.services.NeshanMapStyle;
import org.neshan.services.NeshanServices;
import org.neshan.styles.AnimationStyle;
import org.neshan.styles.AnimationStyleBuilder;
import org.neshan.styles.AnimationType;
import org.neshan.styles.MarkerStyle;
import org.neshan.styles.MarkerStyleCreator;
import org.neshan.ui.ClickData;
import org.neshan.ui.ClickType;
import org.neshan.ui.MapEventListener;
import org.neshan.ui.MapView;
import org.neshan.utils.BitmapUtils;
import org.neshan.vectorelements.Marker;

public class AddAndRemoveMarker extends AppCompatActivity {

    final int BASE_MAP_INDEX = 0;
    MapView map;
    VectorElementLayer markerLayer;
    Marker marker;
    AnimationStyle animSt;
    Marker tempMarker=null;

//TODO:CHANGE DAY TO NIGHT
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_add_and_remove_marker);
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        initLayoutReferences();
    }

    private void initLayoutReferences()
    {
        initViews();
        initMap();

        map.setMapEventListener(new MapEventListener()
        {
            @Override
            public void onMapClicked(ClickData mapClickInfo)
            {
                super.onMapClicked(mapClickInfo);

                if (mapClickInfo.getClickType() == ClickType.CLICK_TYPE_LONG)
                {
                    LngLat clickedLocation = mapClickInfo.getClickPos();
                    addMarker(clickedLocation,0);
                }
            }
        });
    }
    private void initViews()
    {
        map = findViewById(R.id.map3);
    }
    private void initMap()
    {
        markerLayer = NeshanServices.createVectorElementLayer();
        map.getLayers().add(markerLayer);

        map.getOptions().setZoomRange(new Range(4.5f, 18f));
        Layer baseMap = NeshanServices.createBaseMap(NeshanMapStyle.STANDARD_DAY, getCacheDir() + "/baseMap", 10);
        map.getLayers().insert(BASE_MAP_INDEX, baseMap);
        map.setFocalPointPosition(new LngLat(51.330743, 35.767234), 0);
        map.setZoom(14, 0);
    }

    private void addMarker(LngLat loc, long id)
    {
        AnimationStyleBuilder animStBl = new AnimationStyleBuilder();
        animStBl.setFadeAnimationType(AnimationType.ANIMATION_TYPE_SMOOTHSTEP);
        animStBl.setSizeAnimationType(AnimationType.ANIMATION_TYPE_SPRING);
        animStBl.setPhaseInDuration(0.5f);
        animStBl.setPhaseOutDuration(0.5f);
        animSt = animStBl.buildStyle();
        MarkerStyleCreator markStCr = new MarkerStyleCreator();
        markStCr.setSize(30f);
        markStCr.setBitmap(BitmapUtils.createBitmapFromAndroidBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.ic_marker_blue)));
        markStCr.setAnimationStyle(animSt);
        MarkerStyle markSt = markStCr.buildStyle();
        marker = new Marker(loc, markSt);
        marker.setMetaDataElement("id", new Variant(id));
        if(tempMarker==null)
        {
        tempMarker=marker;
        markerLayer.add(marker);
        }
        else
        {
            markerLayer.remove(tempMarker);
            tempMarker=marker;
            markerLayer.add(marker);
        }
    }
}
