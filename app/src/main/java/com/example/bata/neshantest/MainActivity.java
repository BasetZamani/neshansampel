package com.example.bata.neshantest;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import org.neshan.core.LngLat;
import org.neshan.core.Range;
import org.neshan.graphics.ARGB;
import org.neshan.layers.Layer;
import org.neshan.layers.VectorElementLayer;
import org.neshan.services.NeshanMapStyle;
import org.neshan.services.NeshanServices;
import org.neshan.styles.LabelStyle;
import org.neshan.styles.LabelStyleCreator;
import org.neshan.styles.MarkerStyle;
import org.neshan.styles.MarkerStyleCreator;
import org.neshan.ui.MapView;
import org.neshan.utils.BitmapUtils;
import org.neshan.vectorelements.Label;
import org.neshan.vectorelements.Marker;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private MapView map;
    final int BASE_MAP_INDEX = 0;
    VectorElementLayer markerLayer;
    Marker marker;
    List<LngLat> aa=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        aa.add(new LngLat(46.988953, 35.311325));

        aa.add(new LngLat(46.988944, 35.311197));
    }
    @Override
    protected void onStart() {
        super.onStart();
        initLayoutReferences();
    }
    private void initLayoutReferences() {
        initViews();
        initMap(new LngLat(46.988731, 35.310842));
    }

    private void initViews(){
        map = findViewById(R.id.map);
    }
    private void initMap(LngLat loc) {
        markerLayer = NeshanServices.createVectorElementLayer();
        map.getLayers().add(markerLayer);

        map.getOptions().setZoomRange(new Range(4.5f, 18f));
        Layer baseMap = NeshanServices.createBaseMap(NeshanMapStyle.STANDARD_DAY, getCacheDir() + "/baseMap", 10);
        map.getLayers().insert(BASE_MAP_INDEX, baseMap);

        map.setFocalPointPosition(loc, 0);
        map.setZoom(14, 0);
        MarkerStyleCreator markStCr = new MarkerStyleCreator();
        markStCr.setSize(30f);
        markStCr.setBitmap(BitmapUtils.createBitmapFromAndroidBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.ic_marker)));
        MarkerStyle markSt = markStCr.buildStyle();
        marker = new Marker(loc, markSt);
        markerLayer.add(marker);
        addPoint(aa);
    }


    public void addPoint(List<LngLat> paths)
    {
        List<Double> aaaa=new ArrayList<>();
        double x1 =paths.get(0).getX();
        double x2 =paths.get(1).getX();
        double temp=0;
        if(x1>x2)
        {
           temp=x2;
        }
        else
            {temp=x1;}

        while (x1>temp)
        {
            aaaa.add(temp);
            temp+=0.000001;
        }
        double x3=Math.round((x1-x2) * 10000.0) / 10000.0;
        double x4=0;

    }

}