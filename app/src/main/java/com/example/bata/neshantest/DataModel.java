package com.example.bata.neshantest;

public class DataModel
{
   private String nameStrit;
   private String dastorAmale;
   private String tolStep;
   private String timeStep;
   private String gahat;

    public DataModel(String nameStrit, String dastorAmale, String tolStep, String timeStep, String gahat) {
        this.nameStrit = nameStrit;
        this.dastorAmale = dastorAmale;
        this.tolStep = tolStep;
        this.timeStep = timeStep;
        this.gahat = gahat;
    }

    public String getNameStrit() {
        return nameStrit;
    }

    public void setNameStrit(String nameStrit) {
        this.nameStrit = nameStrit;
    }

    public String getDastorAmale() {
        return dastorAmale;
    }

    public void setDastorAmale(String dastorAmale) {
        this.dastorAmale = dastorAmale;
    }

    public String getTolStep() {
        return tolStep;
    }

    public void setTolStep(String tolStep) {
        this.tolStep = tolStep;
    }

    public String getTimeStep() {
        return timeStep;
    }

    public void setTimeStep(String timeStep) {
        this.timeStep = timeStep;
    }

    public String getGahat() {
        return gahat;
    }

    public void setGahat(String gahat) {
        this.gahat = gahat;
    }
}
