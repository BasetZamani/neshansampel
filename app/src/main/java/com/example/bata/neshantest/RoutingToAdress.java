package com.example.bata.neshantest;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.bata.neshantest.network.PubKeyManager;
import com.example.bata.neshantest.task.PolylineEncoding;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.single.PermissionListener;

import org.json.JSONArray;
import org.json.JSONObject;
import org.neshan.core.LngLat;
import org.neshan.core.LngLatVector;
import org.neshan.core.Range;
import org.neshan.core.Variant;
import org.neshan.geometry.LineGeom;
import org.neshan.graphics.ARGB;
import org.neshan.layers.Layer;
import org.neshan.layers.VectorElementLayer;
import org.neshan.services.NeshanMapStyle;
import org.neshan.services.NeshanServices;
import org.neshan.styles.AnimationStyle;
import org.neshan.styles.AnimationStyleBuilder;
import org.neshan.styles.AnimationType;
import org.neshan.styles.LineStyle;
import org.neshan.styles.LineStyleCreator;
import org.neshan.styles.MarkerStyle;
import org.neshan.styles.MarkerStyleCreator;
import org.neshan.ui.ClickData;
import org.neshan.ui.ClickType;
import org.neshan.ui.MapEventListener;
import org.neshan.ui.MapView;
import org.neshan.utils.BitmapUtils;
import org.neshan.vectorelements.Line;
import org.neshan.vectorelements.Marker;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
public class RoutingToAdress extends AppCompatActivity {


    final int BASE_MAP_INDEX = 0;
    MapView map;
    Button btnover;
    Button btnstep;
    Button btnRouting;
    boolean overview = false;
    List<PolylineEncoding.LatLng> decodedStepByStepPath;
    VectorElementLayer lineLayer;
    VectorElementLayer markerLayer;
    Marker marker;
    Marker tempMarker;
    AnimationStyle animSt;
    LngLat getInBundel;
//-------------------------------------------------------------------------------------------------
    List<DataModel> nameSt=new ArrayList<>();
    List<LngLat> templanlat=new ArrayList<>();
    TextView textView;
//    DataBasetTest mydb;
//-------------------------------------------------------------------------------------------------
    final int REQUEST_CODE = 123;
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 1000;
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 1000;
    private Location userLocation;
    private FusedLocationProviderClient fusedLocationClient;
    private SettingsClient settingsClient;
    private LocationRequest locationRequest;
    private LocationSettingsRequest locationSettingsRequest;
    private LocationCallback locationCallback;
    private String lastUpdateTime;
    private Boolean mRequestingLocationUpdates;
//-------------------------------------------------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // starting app in full screen
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_routing_to_adress);
    }

    @Override
    protected void onStart() {
        super.onStart();
        initLayoutReferences();
        initLocation();
        startReceivingLocationUpdates();
    }

    private void initLayoutReferences() {
        initViews();
         getInBundel=new LngLat(46.987263, 35.310448);
        initMap();
        addMarker(getInBundel, 0);


        map.setMapEventListener(new MapEventListener() {
            @Override
            public void onMapClicked(ClickData mapClickInfo) {
                super.onMapClicked(mapClickInfo);
                if (mapClickInfo.getClickType() == ClickType.CLICK_TYPE_LONG ) {
                    LngLat clickedLocation = mapClickInfo.getClickPos();
                    addMarker(clickedLocation, 1);

                }
            }
        });
    }

    private void initViews(){
        map = findViewById(R.id.map5);
        btnstep=findViewById(R.id.btn_step1);
        btnRouting=findViewById(R.id.btn_routing);
        textView=findViewById(R.id.textView1);
        //TODO :change here

        btnstep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                overview = false;
                drawLineGeom(decodedStepByStepPath);
            }
        });
        btnRouting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                if((markerLayer.getAll().size()==2))
                {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        neshanRoutingApi();
                    }
                });
                }
                else{
                    Toast.makeText(RoutingToAdress.this, "add point or start at your position", Toast.LENGTH_SHORT).show();
                }
//                //****************************************************
//                Cursor cr=mydb.show_all();
//                StringBuilder st=new StringBuilder();
//                while (cr.moveToNext())
//                {
//                    st.append("id="+cr.getString(0)+"X1="+cr.getString(1)+"Y1="+cr.getString(2)+"\n"
//                            +"X2="+cr.getString(3)+"Y2="+cr.getString(4)+"\n"
//                            +"X3="+cr.getString(5)+"Y3="+cr.getString(6)+"\n"
//                    );
//                }
//                AlertDialog.Builder builder= new AlertDialog.Builder(RoutingToAdress.this);
//                builder.setCancelable(true);
//                builder.setMessage(st);
//
//
//                AlertDialog dialog=builder.create();
//                dialog.show();

            }
        });
    }

    private void initMap(){

        lineLayer = NeshanServices.createVectorElementLayer();
        markerLayer = NeshanServices.createVectorElementLayer();
        map.getLayers().add(lineLayer);
        map.getLayers().add(markerLayer);

        map.getOptions().setZoomRange(new Range(4.5f, 18f));
        Layer baseMap = NeshanServices.createBaseMap(NeshanMapStyle.NESHAN, getCacheDir() + "/baseMap", 10);

        map.getLayers().insert(BASE_MAP_INDEX,baseMap);

        map.setFocalPointPosition(getInBundel,0 );
        map.setZoom(14,0);
    }

    private void addMarker(LngLat loc, long id) {
        AnimationStyleBuilder animStBl = new AnimationStyleBuilder();
        animStBl.setFadeAnimationType(AnimationType.ANIMATION_TYPE_SMOOTHSTEP);
        animStBl.setSizeAnimationType(AnimationType.ANIMATION_TYPE_SPRING);
        animStBl.setPhaseInDuration(0.5f);
        animStBl.setPhaseOutDuration(0.5f);
        animSt = animStBl.buildStyle();
        MarkerStyleCreator markStCr = new MarkerStyleCreator();
        markStCr.setSize(30f);
        markStCr.setBitmap(BitmapUtils.createBitmapFromAndroidBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.ic_marker)));
        markStCr.setAnimationStyle(animSt);
        MarkerStyle markSt = markStCr.buildStyle();
        marker = new Marker(loc, markSt);
        if (id==1 && tempMarker==null)
        {
            tempMarker=marker;
            marker.setMetaDataElement("id", new Variant(id));
            markerLayer.add(marker);
        }
        else if (id==0)
        {
            marker.setMetaDataElement("id", new Variant(id));
            markerLayer.add(marker);
        }
        else
            {
                markerLayer.remove(tempMarker);
                tempMarker=marker;
                marker.setMetaDataElement("id", new Variant(id));
                markerLayer.add(marker);
            }
    }

    private void neshanRoutingApi() {
        LngLat firstMarkerpos = markerLayer.getAll().get(1).getGeometry().getCenterPos();
        LngLat secondMarkerpos = markerLayer.getAll().get(0).getGeometry().getCenterPos();
        String requestURL = "https://api.neshan.org/v2/direction?origin=" + firstMarkerpos.getY() + "," + firstMarkerpos.getX() + "&destination=" + secondMarkerpos.getY() + "," + secondMarkerpos.getX();

        TrustManager tm[] = {new PubKeyManager("30820122300d06092a864886f70d01010105000382010f003082010a0282010100b2d2b372f340619bdd691d443d5cc5c4fa458eb02709d232702b29bab76dd91a5fb13de61ba32100604c0071664feb928bafe4226204e605017d92dfbeaff27debf9c9d47709894a53d5717fac9a6c0f562697fc8ffaac1d633fa0c3781bf4d665940340bb603f6b821a460aa730eecb624acc165ab5e765b894938437702cbe582dd038c79c41603034258f675c63beb68b76cb844f916a800d222d5393eead1b1cff218b6a9b7abd71eada18f262b57fd378130bc1dd4ff1558c5d1c1823219b2a35a43cd4c0f178f5b85a00efc7c83dc6cfce8a2a24fba879bc401c276466f0f13fbb16ac70516badb03e1a01676a4a8199be2096f2a09e719de5c084999d0203010001")};
        SSLSocketFactory pinnedSSLSocketFactory = null;
        try {
            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, tm, null);
            pinnedSSLSocketFactory = context.getSocketFactory();
        } catch (NoSuchAlgorithmException | KeyManagementException e) {
            e.printStackTrace();
        }

        RequestQueue requestQueue = Volley.newRequestQueue(this, new HurlStack(null, pinnedSSLSocketFactory));

        StringRequest reverseGeoSearchRequest = new StringRequest(
                Request.Method.GET,
                requestURL,
                new com.android.volley.Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        try {
                            lineLayer.clear();
                            JSONObject obj = new JSONObject(response);
                            JSONArray stepByStepPath = obj.getJSONArray("routes").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getJSONArray("steps");
                            decodedStepByStepPath = new ArrayList<>();
                            for (int i = 0; i < stepByStepPath.length(); i++) {
                                List<PolylineEncoding.LatLng> decodedEachStep = PolylineEncoding.decode(stepByStepPath.getJSONObject(i).getString("polyline"));
                                DataModel temp=new DataModel(stepByStepPath.getJSONObject(i).getString("name"),stepByStepPath.getJSONObject(i).getString("instruction"),
                                        stepByStepPath.getJSONObject(i).getString("distance"),stepByStepPath.getJSONObject(i).getString("duration"),
                                        stepByStepPath.getJSONObject(i).getString("maneuver"));
                                nameSt.add(temp);
                                decodedStepByStepPath.addAll(decodedEachStep);
                            }

                            drawLineGeom(decodedStepByStepPath);

                        } catch (Exception e) {

                            Log.e("error", e.getMessage());
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<>();
                // TODO: replace "YOUR_API_KEY" with your api key
                params.put("Api-Key", "service.VNlPhrWb3wYRzEYmstQh3GrAXyhyaN55AqUSRR3V");
                return params;
            }
        };
        requestQueue.add(reverseGeoSearchRequest);
    }

    public LineGeom drawLineGeom(List<PolylineEncoding.LatLng> paths) {
        //TODO:change color add label and more
        lineLayer.clear();
        LngLatVector lngLatVector = new LngLatVector();

        for (PolylineEncoding.LatLng path : paths)
        {
            templanlat.add(new LngLat(path.lng, path.lat));
            lngLatVector.add(new LngLat(path.lng, path.lat));
        }

        LineGeom lineGeom = new LineGeom(lngLatVector);
        Line line = new Line(lineGeom, getLineStyle());
        lineLayer.add(line);

        mapSetPosition(overview);
        return lineGeom;
    }

    private void mapSetPosition(boolean overview) {
        double centerFirstMarkerX;
        double centerFirstMarkerY;
        if (templanlat!=null)
        {
         centerFirstMarkerX = templanlat.get(0).getX();
         centerFirstMarkerY = templanlat.get(0).getY();

        }
        //TODO :THIS IS PLACE CAMERA START
        else {
         centerFirstMarkerX = markerLayer.getAll().get(1).getGeometry().getCenterPos().getX();
         centerFirstMarkerY = markerLayer.getAll().get(1).getGeometry().getCenterPos().getY();}
        if (overview) {
            double centerFocalPositionX = (centerFirstMarkerX + markerLayer.getAll().get(1).getGeometry().getCenterPos().getX()) / 2;
            double centerFocalPositionY = (centerFirstMarkerY + markerLayer.getAll().get(1).getGeometry().getCenterPos().getY()) / 2;
            map.setFocalPointPosition(new LngLat(centerFocalPositionX, centerFocalPositionY),0.5f );
            map.setZoom(14,0.5f);
        } else {
            map.setFocalPointPosition(new LngLat(centerFirstMarkerX, centerFirstMarkerY),0.5f );
            map.setZoom(18,0.5f);
        }

    }

    private LineStyle getLineStyle()
    {//TODO:CHANGE color of line
        LineStyleCreator lineStCr = new LineStyleCreator();
        lineStCr.setColor(new ARGB((short) 2, (short) 136, (short) 14, (short)79));
        lineStCr.setWidth(10f);
        lineStCr.setStretchFactor(0f);
        return lineStCr.buildStyle();
    }



        //------------------------------------------------------------------------------------------
   @Override
    protected void onResume(){
        super.onResume();
        startLocationUpdates();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
    }


    private void initLocation()
    {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        settingsClient = LocationServices.getSettingsClient(this);
        locationCallback = new LocationCallback()
        {
            @Override
            public void onLocationResult(LocationResult locationResult)
            {
                super.onLocationResult(locationResult);
                userLocation = locationResult.getLastLocation();
                lastUpdateTime = DateFormat.getTimeInstance().format(new Date());

                onLocationChange();
            }
        };
        mRequestingLocationUpdates = false;
        locationRequest = new LocationRequest();
        locationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        locationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(locationRequest);
        locationSettingsRequest = builder.build();
    }


    private void startLocationUpdates()
    {
        settingsClient
                .checkLocationSettings(locationSettingsRequest)
                .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>()
                {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse)
                    {
                        fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());
                        onLocationChange();
                    }
                })
                .addOnFailureListener(this, new OnFailureListener()
                {
                    @Override
                    public void onFailure(@NonNull Exception e)
                    {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode)
                        {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                try
                                {
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(RoutingToAdress.this, REQUEST_CODE);
                                }
                                catch (IntentSender.SendIntentException sie)
                                {
                                }
                                break;
                        }

                        onLocationChange();
                    }
                });
    }

    public void stopLocationUpdates()
    {
        fusedLocationClient
                .removeLocationUpdates(locationCallback)
                .addOnCompleteListener(this, new OnCompleteListener<Void>()
                {
                    @Override
                    public void onComplete(@NonNull Task<Void> task)
                    {
                    }
                });
    }
    public void startReceivingLocationUpdates()
    {
        //TODO: SHOW DIALOG AND TEL USER WHY WE WANT PERMISSION
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response)
                    {
                        mRequestingLocationUpdates = true;
                        startLocationUpdates();
                    }
                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response)
                    {
                        if (response.isPermanentlyDenied())
                        {
                            //TODO :SHOW DIALOG AND TEL USER TO GET Permission in setting and send user to there
                            //TODO :if user want to not get us permission we let him/her do it
                            openSettings();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(com.karumi.dexter.listener.PermissionRequest permission, PermissionToken token)
                    {
                        token.continuePermissionRequest();
                    }

                }).check();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        switch (requestCode)
        {
            case REQUEST_CODE:
                switch (resultCode)
                {
                    case Activity.RESULT_OK:
                        break;
                    case Activity.RESULT_CANCELED:
                        mRequestingLocationUpdates = false;
                        break;
                }
                break;
        }
    }

    private void openSettings()
    {
        Intent intent = new Intent();
        intent.setAction(
                Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package",
                BuildConfig.APPLICATION_ID, null);
        intent.setData(uri);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void onLocationChange()
    {
//        if(userLocation != null)
//        {
//            addMarker(new LngLat(userLocation.getLongitude(), userLocation.getLatitude()),1);
//        }

                //TODO in ther we have to locate and clear
        if(userLocation != null)
        {


            if (templanlat.size()!=0){

               double userX=Math.round(userLocation.getLongitude() * 10000.0) / 10000.0;
                double userY=Math.round(userLocation.getLatitude() * 10000.0) / 10000.0;

                double lineX1=Math.round(templanlat.get(0).getX() * 10000.0) / 10000.0;
                double lineY1=Math.round(templanlat.get(0).getY() * 10000.0) / 10000.0;

                double lineX2=Math.round(templanlat.get(1).getX() * 10000.0) / 10000.0;
                double lineY2=Math.round(templanlat.get(1).getY() * 10000.0) / 10000.0;
//                mydb.insert(userX,userY,lineX1,lineY1,lineX2,lineY2);

                addMarker(new LngLat(userLocation.getLongitude(),userLocation.getLatitude()),1);

                if ((userX>=lineX1 && userX<=lineX2)||(userX<=lineX1 && userX>=lineX2))
                {
                    if ((userY>=lineY1 && userY<=lineY2)||(userY<=lineY1&& userY>=lineY2))
                    {
                        textView.setText("you are on line ");
//                        Toast.makeText(this, , Toast.LENGTH_SHORT).show();
//                         a11= new LngLat(userLocation.getLongitude(),userLocation.getLatitude());
//                             cleare(null);
                    }
                    else
                    {
                        textView.setText("you are not in  line");
//                        Toast.makeText(this, "you are not in  line ", Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                        textView.setText("you are not in  line");

//                            Toast.makeText(this, "you are not in  line ", Toast.LENGTH_SHORT).show();
                }
//                double a= userLocation.getLongitude();
//                double b= templanlat.get(0).getX();
//                double x1 = Math.round(a * 10000.0) / 10000.0;
//                double x2 = Math.round(b * 10000.0) / 10000.0;
//                double t= (x1-x2);
//                if(t>-1 && t<1){
            }

        }


    }
//    LngLat a11;
    public void focusOnUserLocation1(View view)
    {

        if(userLocation != null)
        {
            addMarker(new LngLat(userLocation.getLongitude(), userLocation.getLatitude()),1);
            map.setFocalPointPosition(
                    new LngLat(userLocation.getLongitude(), userLocation.getLatitude()), 0.25f);
            map.setZoom(15, 0.25f);
        }
    }
        //------------------------------------------------------------------------------------------

    public void cleare(View v)
    {

        //TODO: camera Tile Bearing
        //  map.setBearing();
        // map.setBearing(30,0f);
        // map.setTilt(30,0f);
        //TODO:change camera
        if (templanlat.size()==0 || templanlat.size()==1)
        {
            Toast.makeText(this, "you have to Routing first" , Toast.LENGTH_SHORT).show();
        }
        else {

        List<LngLat> temp=new ArrayList<>();
//        temp.add(a11);
        for (int i=1;i<templanlat.size();i++)
        {
            temp.add(templanlat.get(i));
        }
        templanlat.clear();
        templanlat=temp;
        drawLineGeom1(templanlat);

        }

    }
    public LineGeom drawLineGeom1(List<LngLat> paths) {
        //TODO:change color add label and more
        lineLayer.clear();
        LngLatVector lngLatVector = new LngLatVector();

        for (LngLat path : paths)
        {
            lngLatVector.add(new LngLat(path.getX(), path.getY()));
        }

        LineGeom lineGeom = new LineGeom(lngLatVector);
        Line line = new Line(lineGeom, getLineStyle());
        lineLayer.add(line);
        mapSetPosition(overview);
        return lineGeom;
    }



}
